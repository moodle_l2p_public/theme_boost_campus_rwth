// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Floating Edit Button
 *
 * @package    theme_boost_campus_rwth
 * @copyright  2018 Markus Offermann, RWTH Aachen <offermann@itc.rwth-aachen.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery'], function($) {
     "use strict";

     /**
      * Initialising.
     */
 	// Var oButton = $('button[id^="single_button"]');

	function editButton() {
		var lastScroll = 0;
		var currentScroll = 0;
		var scrollUp = false;
		var originalScroll = null;
		var scrollThreshold = 30;

			// $(window).scrollTop(sessionStorage.getItem('scrollTop'));

		var init = function() {
			$('#page-header div.singlebutton button[id^="single_button"]').click(function() {
				sessionStorage.setItem('jump', 'true');
			});

			$(window).resize(function() {
				if ($('html').innerWidth() <= 768) {
					$('#page-header div.singlebutton button[id^="single_button"]:eq(1)').addClass('float-btn-show-left');

				}
			});


			if ($('#page-header div.singlebutton button[id^="single_button"]:eq(1)').length == 1) {
				$('#page-header div.singlebutton button[id^="single_button"]:eq(1)').addClass('float-btn-show-left');
			}


			$(window).scroll(function() {
				currentScroll = $(window).scrollTop();
				storeScroll(currentScroll);
				scrollEvent();
			});

			var jump = sessionStorage.getItem('jump');
			if (jump === 'true') {
				$(window).scrollTop(sessionStorage.getItem('scrollTop'));
			}
			sessionStorage.setItem('jump', 'false');

		};

		var buttonAction = function(b) {
			if (b) {
				$('#page-header div.singlebutton button[id^="single_button"]')
				.removeClass('float-btn-hide').addClass('float-btn-show');
				// $('body').css('overscrollBehaviour', 'none');
			} else {
				$('#page-header div.singlebutton button[id^="single_button"]')
				.removeClass('float-btn-show').addClass('float-btn-hide');
			}
		};
		var getScrollDelta = function() {
			var tmp = originalScroll - currentScroll;
			if (tmp > 0) {
				return tmp;
			} else {
				return -tmp;
			}
		};

		var scrollEvent = function() {
			if (originalScroll === null) {
				originalScroll = $(window).scrollTop();
			} else {
				if (isScrollUp()) {
					if (getScrollDelta() > scrollThreshold) {
						buttonAction(true);
						originalScroll = $(window).scrollTop();
					}
				} else {
					if (getScrollDelta() > scrollThreshold) {
						buttonAction(false);
						originalScroll = $(window).scrollTop();
					}
				}
			}
		};

		var isScrollUp = function(scrollUp) {
			// CurrentScroll = $(window).scrollTop();
			if (currentScroll <= 0) {
				scrollUp = true;
				return scrollUp;
			} else {
				if (currentScroll < lastScroll) {
					scrollUp = true;
				} else {
					scrollUp = false;
				}
				lastScroll = currentScroll;
				return scrollUp;
			}
		};

		var storeScroll = function(currentScroll) {

			sessionStorage.setItem('scrollTop', currentScroll);
		};

		init();
	}

     return {

        init: function() {

			editButton();

        }
    };
});
