<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// NOTE: no MOODLE_INTERNAL test here, this file may be required by behat before including /config.php.

require_once(__DIR__ . '/../../../../auth/tests/behat/behat_auth.php');

class behat_theme_boost_campus_rwth_behat_auth extends behat_auth {

    public function i_log_in_as($username) {
        // In the mobile app the required tasks are different.
        if ($this->is_in_app()) {
            $this->execute('behat_app::login', [$username]);
            return;
        }

        // Visit login page.
        $this->getSession()->visit($this->locate_path('theme/boost_campus_rwth/login.php?redirect=0'));

        // Enter username and password.
        $this->execute('behat_forms::i_set_the_field_to', array('Username', $this->escape($username)));
        $this->execute('behat_forms::i_set_the_field_to', array('Password', $this->escape($username)));

        // Press log in button, no need to check for exceptions as it will checked after this step execution.
        $this->execute('behat_forms::press_button', get_string('login', 'theme_boost_campus_rwth'));
    }
}
