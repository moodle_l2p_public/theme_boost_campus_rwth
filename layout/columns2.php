<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus - Layout file.
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2017 Kathrin Osswald, Ulm University <kathrin.osswald@uni-ulm.de>
 *            2018 Tim Schroeder, RWTH Aachen University <t.schroeder@rwth-aachen.de>
 *            based on code from theme_boost by Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
user_preference_allow_ajax_update('theme_boost_campus_infobanner_dismissed', PARAM_BOOL);
require_once($CFG->libdir . '/behat/lib.php');
require_once($CFG->dirroot . '/theme/boost_campus/locallib.php');
require_once($CFG->dirroot . '/theme/boost_campus_rwth/locallib.php');

global $DB, $COURSE;

if (isloggedin()) {
    $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
} else {
    $navdraweropen = false;
}
$extraclasses = [];
if ($navdraweropen) {
    $extraclasses[] = 'drawer-open-left';
}
$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$blockshtml = $OUTPUT->blocks('side-pre');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();
// MODIFICATION START
// Disabling 'catchendkey', 'catchcmdarrowdown' and 'catchctrlarrowdown',
// regardless of parent theme settings.
$catchshortcuts = array();
// MODIFICATION END.

// MODIFICATION START
// Enabling 'darknavbar', regardless of parent theme settings.
$darknavbar = true;
// MODIFICATION END.

// MODIFICATION START
// Disabling 'navdrawerfullwidth', regardless of parent theme settings.
$navdrawerfullwidth = 'no';
// MODIFICATION END.

// MODIFICATION START
// Use original Boost Campus back-to-top- button.
$bcbttbutton = true;
// MODIFICATION END.

$perpinfobannershowonselectedpage = false;
$timedinfobannershowonselectedpage = false;


$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'hasblocks' => $hasblocks,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => $navdraweropen,
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
    // Add Boost Campus realated values to the template context.
    'catchshortcuts' => json_encode($catchshortcuts),
    'navdrawerfullwidth' => $navdrawerfullwidth,
    'darknavbar' => $darknavbar,
    'perpinfobannershowonselectedpage' => $perpinfobannershowonselectedpage,
    'timedinfobannershowonselectedpage' => $timedinfobannershowonselectedpage,
    'bcbttbutton' => $bcbttbutton
];

// Settings for perpetual information banner.
$perpibenable = get_config('theme_boost_campus_rwth', 'perpibenable');

if ($perpibenable) {
    $perpibcontent = format_text(get_config('theme_boost_campus_rwth', 'perpibcontent'), FORMAT_HTML);
    // Result of multiselect is a string divided by a comma, so exploding into an array.
    $perpibshowonpages = explode(",", get_config('theme_boost_campus_rwth', 'perpibshowonpages'));
    $perpibcss = get_config('theme_boost_campus_rwth', 'perpibcss');
    $perpibdismiss = get_config('theme_boost_campus_rwth', 'perpibdismiss');
    $perbibconfirmdialogue = get_config('theme_boost_campus_rwth', 'perpibconfirm');
    $perbibuserprefdialdismissed = get_user_preferences('theme_boost_campus_infobanner_dismissed');

    $perpinfobannershowonselectedpage = theme_boost_campus_show_banner_on_selected_page($perpibshowonpages,
            $perpibcontent, $PAGE->pagelayout, $perbibuserprefdialdismissed);

    // Add the variables to the templatecontext array.
    $templatecontext['perpibcontent'] = $perpibcontent;
    $templatecontext['perpibcss'] = $perpibcss;
    $templatecontext['perpibdismiss'] = $perpibdismiss;
    $templatecontext['perpinfobannershowonselectedpage'] = $perpinfobannershowonselectedpage;
    $templatecontext['perbibconfirmdialogue'] = $perbibconfirmdialogue;
}

// Settings for time controlled information banner.
$timedibenable = get_config('theme_boost_campus_rwth', 'timedibenable');

if ($timedibenable) {
    $timedibcontent = format_text(get_config('theme_boost_campus_rwth', 'timedibcontent'), FORMAT_HTML);
    // Result of multiselect is a string divided by a comma, so exploding into an array.
    $timedibshowonpages = explode(",", get_config('theme_boost_campus_rwth', 'timedibshowonpages'));
    $timedibcss = get_config('theme_boost_campus_rwth', 'timedibcss');
    $timedibstartsetting = get_config('theme_boost_campus_rwth', 'timedibstart');
    $timedibendsetting = get_config('theme_boost_campus_rwth', 'timedibend');
    // Get the current server time.
    $now = (new DateTime("now", core_date::get_server_timezone_object()))->getTimestamp();

    $timedinfobannershowonselectedpage = theme_boost_campus_show_timed_banner_on_selected_page($now, $timedibshowonpages,
            $timedibcontent, $timedibstartsetting, $timedibendsetting, $PAGE->pagelayout);

    // Add the variables to the templatecontext array.
    $templatecontext['timedibcontent'] = $timedibcontent;
    $templatecontext['timedibcss'] = $timedibcss;
    $templatecontext['timedinfobannershowonselectedpage'] = $timedinfobannershowonselectedpage;
}

// Use the returned value from theme_boost_campus_get_modified_flatnav_defaulthomepageontop as the template context.
$templatecontext['flatnavigation'] = theme_boost_campus_process_flatnav($PAGE->flatnav);
// MODIFICATION START
// Enabling 'showsettingsincourse', regardless of parent theme settings.
// MODIFICATION END
if ($PAGE->bodyid != 'page-contentbank') {
    // Context value for requiring incoursesettings.js.
    $templatecontext['incoursesettings'] = true;
    // Add the returned value from theme_boost_campus_get_incourse_settings to the template context.
    $templatecontext['node'] = theme_boost_campus_rwth_get_incourse_settings();
    // Add the returned value from theme_boost_campus_get_incourse_activity_settings to the template context.
    $templatecontext['activitynode'] = theme_boost_campus_rwth_get_incourse_activity_settings();
}
// MODIFICATION START
$templatecontext['clampjs'] = file_get_contents($CFG->dirroot.'/theme/boost_campus_rwth/javascript/clamp.min.js');
// MODIFICATION END
ob_start();

// Require additional layout files.
// Add footer blocks and standard footer.
require_once(__DIR__ . '/includes/footer.php');

$pagebottomelements = ob_get_clean();

// If there isn't anything in the buffer, set the additional layouts string to an empty string to avoid problems later on.
if ($pagebottomelements == false) {
    $pagebottomelements = '';
}
// Add the additional layouts to the template context.
$templatecontext['pagebottomelements'] = $pagebottomelements;

// Add space before sectionname in navbar for subsections in onetopic format to show the structure better
$format = course_get_format($COURSE)->get_format();
if ($format === "onetopic") {
    $courseid = optional_param('id', -1, PARAM_INT);
    if ($courseid != -1) {
        $flatnavigation = $PAGE->flatnav;
        $sql = 'SELECT sectionid, value FROM {course_format_options} WHERE format = "onetopic" AND name = "level" AND sectionid > 3 AND courseid = :courseid';
        $levels = $DB->get_records_sql($sql, ['courseid' => $COURSE->id]);
        foreach ($levels as $level) {
            $section = $flatnavigation->get($level->sectionid);
            if ($section) {
                $section->level = $level->value;
            }
        }
    }
}

// Render colums2.mustache from boost_campus.
echo $OUTPUT->render_from_template('theme_boost_campus/columns2', $templatecontext);
//Inster the script for the floating Edit Button
$PAGE->requires->js_call_amd("theme_boost_campus_rwth/floatingeditbutton", "init");
// Require additional layout files.
// Add footer blocks and standard footer.
// MODIFICATION START
// Disabling 'imageareaitems', regardless of parent theme settings.
// Disabling 'footnote', regardless of parent theme settings.
// MODIFICATION END.
