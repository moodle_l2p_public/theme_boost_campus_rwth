<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Login Layout file.
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2017 Kathrin Osswald, Ulm University <kathrin.osswald@uni-ulm.de>
 *            2018 Steffen Schaffert, RWTH Aachen University <schaffert@itc.rwth-aachen.de>
 *            based on code from theme_boost by Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// Redirect user to frontpage if configured.
if (get_config('theme_boost_campus_rwth', 'loginpage_redirect_to_frontpage')) {
    $redirect = optional_param('redirect', true, PARAM_BOOL);
    if ($redirect) {
        redirect(new moodle_url('/'));
    }
}

$bodyattributes = $OUTPUT->body_attributes();

$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes
];

ob_start();

// Add a copyright notice, if configured
require_once(__DIR__ . '/includes/copyright_notice.php');

// Render footer used for the rest of the page here as well
// MOFIFICATION START.
// Include own layout file for the footnote region.
// The theme_boost/login template already renders the standard footer.
// The footer blocks and the image area are currently not shown on the login page.
// Here, we will add the footnote only.
// Get footnote config.
$footnote = get_config('theme_boost_campus', 'footnote');
if (!empty($footnote)) {
    // Add footnote layout file.
    require_once($CFG->dirroot . '/theme/boost_campus/layout/includes/footnote.php');
}

// Disable forcelogin 
$backup = $CFG->forcelogin;
$CFG->forcelogin = '0';

require_once($CFG->dirroot . '/theme/boost_campus_rwth/layout/includes/footer.php');

// Reset forcelogin.
$CFG->forcelogin = $backup;

$pagebottomelements = ob_get_clean();

// If there isn't anything in the buffer, set the additional layouts string to an empty string to avoid problems later on.
if ($pagebottomelements == false) {
    $pagebottomelements = '';
}
// Add the additional layouts to the template context.
$templatecontext['pagebottomelements'] = $pagebottomelements;
// Render colums2.mustache from boost_campus.
echo $OUTPUT->render_from_template('theme_boost/login', $templatecontext);
//Inster the script for the floating Edit Button
$PAGE->requires->js_call_amd("theme_boost_campus_rwth/floatingeditbutton", "init");
// MODIFICATION END.
