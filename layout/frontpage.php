<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus - Frontpage Layout file.
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2019 Tim Schroeder, RWTH Aachen University <t.schroeder@rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/behat/lib.php');

$edit = optional_param('edit', null, PARAM_BOOL);

// Add button for editing page
$params = array('edit' => !$edit);
if (!isset($USER->editing)) {
    $USER->editing = 0;
}
if ($PAGE->user_allowed_editing()) {
    if ($edit === 1 && confirm_sesskey()) {
        $USER->editing = 1;
    } else if ($edit === 0) {
        $USER->editing = 0;
    }
}
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button($PAGE->url);
    $PAGE->set_button($buttons);
}

$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();

$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'incidentblocks' => $OUTPUT->blocks('above-content'),
    'sidepreblocks' => $OUTPUT->blocks('side-pre'),
    'loginmobileblocks' => $OUTPUT->blocks('login-mobile'),
    'logindesktopblocks' => $OUTPUT->blocks('login-desktop'),
    'bodyattributes' => $OUTPUT->body_attributes(''),
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
    // Add Boost Campus realated values to the template context.
    'darknavbar' => true,
    'pageheadingbutton' => $OUTPUT->page_heading_button()
];

// Require additional layout files.
ob_start();
require_once(__DIR__ . '/includes/copyright_notice.php');
$copyright = ob_get_clean();
ob_start();
require_once(__DIR__ . '/includes/footer.php');
$footer = ob_get_clean();

// If there isn't anything in the buffer, set the additional layouts string to an empty string to avoid problems later on.
if ($copyright == false) {
    $copyright = '';
}
if ($footer == false) {
    $footer = '';
}

// Add the additional layouts to the template context.
$templatecontext['copyright'] = $copyright;
$templatecontext['footer'] = $footer;

echo $OUTPUT->render_from_template('theme_boost_campus_rwth/frontpage', $templatecontext);

$PAGE->requires->js_call_amd("theme_boost_campus_rwth/floatingeditbutton", "init");