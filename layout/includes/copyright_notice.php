<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Layout file for copyright notice.
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2018 Steffen Schaffert, RWTH Aachen University <schaffert@itc.rwth-aachen.de>
 *            based on code from theme_boost_campus by Kathrin Osswald
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($PAGE->bodyid == 'page-theme-boost_campus_rwth-login') {
    $copyrightnotice = get_config('theme_boost_campus_rwth', 'loginpage_backgroundimage_copyright');
} else if ($PAGE->bodyid == 'page-site-index') {
    $copyrightnotice = get_config('theme_boost_campus_rwth', 'frontpage_backgroundimage_copyright');
}

// Should not be empty (or contain only empty html tags)
if (!html_is_blank($copyrightnotice)) {
    // Add information to context
    $templatecontext['copyright'] = format_text($copyrightnotice);

    echo $OUTPUT->render_from_template('theme_boost_campus_rwth/copyright_notice', $templatecontext);
}
