theme_boost_campus_rwth
===============================

Moodle Boost Campus child theme used by RWTH Aachen.


Requirements
------------

This plugin requires Moodle 3.4+


Installation
------------

Install the plugin like any other theme to folder
/theme/boost_campus_rwth

See http://docs.moodle.org/en/Installing_plugins for details on installing Moodle plugins


Settings
----------------

To configure the theme and its behaviour, please visit:
Site administration -> Appearance -> Themes -> Boost Campus RWTH.

There, you find multiple settings tabs:

### 1. Tab "General Settings"

In this tab there are the following settings:

#### Theme presets

##### Theme preset

This setting is already available in the Moodle core theme Boost. For more information how to use it, please have a look at the official Moodle documentation: http://docs.moodle.org/en/Boost_theme

##### Additional theme preset files

This setting is already available in the Moodle core theme Boost. For more information how to use it, please have a look at the official Moodle documentation: http://docs.moodle.org/en/Boost_theme

#### Brand colors

##### Brand color

This setting is already available in the Moodle core theme Boost. For more information how to use it, please have a look at the official Moodle documentation: http://docs.moodle.org/en/Boost_theme

##### Brand success color

This color is used for example in regards to form validations.

##### Brand info color

This color is used for example for availability information of course activities or resources.

##### Brand warning color

This color is used for example for warning texts.

##### Brand danger color

This color is used for example in regards to form validations.


### 2. Tab "Advanced Settings"

In this tab there are the following settings:

#### Raw initial SCSS

This setting is already available in the Moodle core theme Boost. For more information how to use it, please have a look at the official Moodle documentation: http://docs.moodle.org/en/Boost_theme

#### Raw SCSS

This setting is already available in the Moodle core theme Boost. For more information how to use it, please have a look at the official Moodle documentation: http://docs.moodle.org/en/Boost_theme


### 3. Tab "Frontpage Settings"

In this tab there are the following settings:

#### Copyright notice for the background image

Here you can specify a text (e.g. a copyright notice) that will appear in the bottom right of the frontpage.

### 4. Tab "Login Page Settings"

In this tab there are the following settings:

#### Copyright notice for the background image

Here you can specify a text (e.g. a copyright notice) that will appear in the bottom right of the login page.

### Parent theme settings

This theme ignores virtually all settings of its parent theme Boost Campus. It has these settings hardcoded internally instead.


Copyright
---------

RWTH Aachen
IT-Center
IT-PFL
Tim Schroeder
Steffen Schaffert
Ramona Renner
