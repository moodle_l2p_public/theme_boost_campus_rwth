<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Locallib file
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2018 Steffen Schaffert, RWTH Aachen University <schaffert@itc.rwth-aachen.de>
 *            2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 defined('MOODLE_INTERNAL') || die();

/**
 * Provides the node for the in-course course or activity settings.
 *
 * @return navigation_node.
 */
function theme_boost_campus_rwth_get_incourse_settings() {
    global $COURSE, $PAGE;
    // Initialize the node with false to prevent problems on pages that do not have a courseadmin node.
    $node = false;
    // MODIFICATION START.
    // Treat showsettingsincourse as enabled, regardless of the parent theme settings.
    // MODIFICATION END.
    // Only search for the courseadmin node if we are within a course or a module context.
    if ($PAGE->context->contextlevel == CONTEXT_COURSE || $PAGE->context->contextlevel == CONTEXT_MODULE) {
        // Get the courseadmin node for the current page.
        $node = $PAGE->settingsnav->find('courseadmin', navigation_node::TYPE_COURSE);
        // Check if $node is not empty for other pages like for example the langauge customization page.
        if (!empty($node)) {
            // MODIFICATION START.
            // Add these to the $node, regardless of the parent theme settings.
            if (!is_role_switched($COURSE->id)) {
            // MODIFICATION END.
                // Build switch role link
                // We could only access the existing menu item by creating the user menu and traversing it.
                // So we decided to create this node from scratch with the values copied from Moodle core.
                $roles = get_switchable_roles($PAGE->context);
                if (is_array($roles) && (count($roles) > 0)) {
                    // Define the properties for a new tab.
                    $properties = array('text' => get_string('switchroleto', 'theme_boost_campus'),
                                        'type' => navigation_node::TYPE_CONTAINER,
                                        'key'  => 'switchroletotab');
                    // Create the node.
                    $switchroletabnode = new navigation_node($properties);
                    // Add the tab to the course administration node.
                    $node->add_node($switchroletabnode);
                    // Add the available roles as children nodes to the tab content.
                    foreach ($roles as $key => $role) {
                        $properties = array('action' => new moodle_url('/course/switchrole.php',
                            array('id'         => $COURSE->id,
                                    'switchrole' => $key,
                                    'returnurl'  => $PAGE->url->out_as_local_url(false),
                                    'sesskey'    => sesskey())),
                                            'type'   => navigation_node::TYPE_CUSTOM,
                                            'text'   => $role);
                        $switchroletabnode->add_node(new navigation_node($properties));
                    }
                }
            }
        }
    }
    return $node;
}

/**
 * Provides the node for the in-course settings for other contexts.
 *
 * @return navigation_node.
 */
function theme_boost_campus_rwth_get_incourse_activity_settings() {
    global $PAGE;
    $context = $PAGE->context;
    $node = false;
    $title = $PAGE->title;
    // MODIFICATION START.
    // Treat setting showsettingsincourse as enabled, regardless of the parent theme settings.
    // MODIFICATION END.
    // Settings belonging to activity or resources.
    if ($context->contextlevel == CONTEXT_MODULE) {
        $node = $PAGE->settingsnav->find('modulesettings', navigation_node::TYPE_SETTING);
    } else if ($context->contextlevel == CONTEXT_COURSECAT) {
        // For course category context, show category settings menu, if we're on the course category page.
        if ($PAGE->pagetype === 'course-index-category') {
            $node = $PAGE->settingsnav->find('categorysettings', navigation_node::TYPE_CONTAINER);
        }
    } else if ($PAGE->url->compare(new moodle_url('/local/rwth_participants_view/participants.php'), URL_MATCH_BASE)) {
        // For course context, show user settings menu, if we're on the participants page.
        $node = $PAGE->settingsnav->find('users', navigation_node::TYPE_CONTAINER);
    } else {
        $node = false;
    }
    return $node;
}
/**
 * Add login page background image to SCSS.
 *
 * @return string
 */
function theme_boost_campus_rwth_get_loginbackgroundimage_scss() {
    global $CFG;
    // Override the SCSS variable for the first (and only) login background image.
    $url = new \moodle_url($CFG->wwwroot.'/theme/boost_campus_rwth/pix/Studierende_SuperC_Hauptgebaeude_03-jpg.JPEG');
    return '$loginbackgroundimage1: "'.$url->out()."\";\n";
}

/**
 * Add frontpage background image to SCSS.
 *
 * @return string
 */
function theme_boost_campus_rwth_get_frontpagebackgroundimage_scss() {
    global $CFG;
    $urls = [
        1280 => new \moodle_url($CFG->wwwroot.'/theme/boost_campus_rwth/pix/frontpage_1280w.jpg'),
        1920 => new \moodle_url($CFG->wwwroot.'/theme/boost_campus_rwth/pix/frontpage_1920w.jpg'),
        2560 => new \moodle_url($CFG->wwwroot.'/theme/boost_campus_rwth/pix/frontpage_2560w.jpg'),
        4096 => new \moodle_url($CFG->wwwroot.'/theme/boost_campus_rwth/pix/frontpage_4096w.jpg')
    ];
    $scss = '';
    foreach ($urls as $res => $url) {
        $scss .= '$frontpagebackgroundimage-'.$res.'w: "'.$url->out().'";';
        $scss .= "\n";
    }
    return $scss;
}
