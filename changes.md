
======CHANGES TO ACCOMODATE RWTH BRANDING & FUNCTIONALITY======

Branding is achieved by implementing a customized login Process.
A customized header holds the RWTH Logo and some tweaks of the
Navigation and Menu Buttons are implemented to better fit the 
functionaliteis to the desired Workflow.


=================CHANGES FOR MOBILE DEVICES====================

beacause the mobile screens were a bit disorientating for long 
course sites, the main focus of these changes is to give a
little more Space for the actual content of the courses (and 
other listed  Information) for mobile devices.


========================ADDED FEATURES=========================

New Login static page added to feature new login process. 
Added header for changed menues, logo and admin/user/login 
features.
Added a copyright marker for the backgroundimage on the frontpage.



============================GENERAL============================

post.scss:
- Added variables for theme colors.
- changed font size to 15px
- chnaged margin on #page to make room for header
- added hover effects
- changed footer layout 
- added line clamping for long titles in 
- fixed atto editor positioning

mobile.scss:
- min-width: 320px & max-width 768px
- changed positioning 
- removed exessive use of position:flex for elements holding content
- removed paddings on inner wrappers
- reduzed margin on inner wrappers 
- smaller mod-indent-size


=============================JAVASCRIPT========================

To get easy acces to the editing tools of moodle at any place 
of a course there is a floating edit Button implemented, wich 
floats directly underneath the header on the right hand of the 
content. to avoid the button abstracting vital content it moves
triggered by scrolling the site to get hidden if you scroll 
down by at least 100px. To show it again you must reverse the
movement of the scroll for the same amount of px.
The line clamping ustilizes some javascript to get the height of
certain h-tags and manipultes them accordingly.


==========================ICONS & PIX===========================

There is an extensive amount of icons used by RWTH to 
distinctivly show the different modules and contenttypes.


===========================LAYOUT================================

To get more use from widescreen deivces there is a two cloumn
design used and defined inside the layouts folder. Also the 
custom frontpages and loginsites layout files reside here.
The course navigation has gone through some changes and gained 
some new entries.

==========================TEMPLATES==============================

Overwritten Templates Core:
- chooser.mustache
- login.mustache
- navbar.mustache
Overwritten/added Templates boost:
- flat_navigation.mustache
- login.mustache
- navbar.mustache
Overwritten Templates boost_campus:
- header.mustache
- footer.mustache
Added Templates:
- copytright_notice.mustache
- frontpage_header.mustache
- frontpage.mustache
- login_header.mustache