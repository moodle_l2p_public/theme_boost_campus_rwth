<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Settings file
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2017 Kathrin Osswald, Ulm University <kathrin.osswald@uni-ulm.de>
 *            2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    // Create settings page with tabs.
    $settings = new theme_boost_admin_settingspage_tabs('themesettingboost_campus_rwth',
        get_string('configtitle', 'theme_boost_campus_rwth', null, true));

    // Create general tab.
    $page = new admin_settingpage('theme_boost_campus_rwth_general', get_string('generalsettings', 'theme_boost', null, true));

    // Settings title to group preset related settings together with a common heading. We don't want a description here.
    $name = 'theme_boost_campus_rwth/presetheading';
    $title = get_string('presetheadingsetting', 'theme_boost_campus', null, true);
    $setting = new admin_setting_heading($name, $title, null);
    $page->add($setting);

    // Replicate the preset setting from theme_boost.
    $name = 'theme_boost_campus_rwth/preset';
    $title = get_string('preset', 'theme_boost', null, true);
    $description = get_string('preset_desc', 'theme_boost', null, true);
    $default = 'default.scss';

    // We list files in our own file area to add to the drop down. We will provide our own function to
    // load all the presets from the correct paths.
    $context = context_system::instance();
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'theme_boost_campus_rwth', 'preset', 0, 'itemid, filepath, filename', false);

    $choices = [];
    foreach ($files as $file) {
        $choices[$file->get_filename()] = $file->get_filename();
    }
    // These are the built in presets from Boost.
    $choices['default.scss'] = 'default.scss';
    $choices['plain.scss'] = 'plain.scss';

    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);


    // Preset files setting.
    $name = 'theme_boost_campus_rwth/presetfiles';
    $title = get_string('presetfiles', 'theme_boost', null, true);
    $description = get_string('presetfiles_desc', 'theme_boost', null, true);

    $setting = new admin_setting_configstoredfile($name, $title, $description, 'preset', 0,
        array('maxfiles' => 20, 'accepted_types' => array('.scss')));
    $page->add($setting);

    // Settings title to group brand color related settings together with a common heading. We don't want a description here.
    $name = 'theme_boost_campus_rwth/brandcolorheading';
    $title = get_string('brandcolorheadingsetting', 'theme_boost_campus', null, true);
    $setting = new admin_setting_heading($name, $title, null);
    $page->add($setting);

    // Variable $brand-color.
    // We use an empty default value because the default colour should come from the preset.
    $name = 'theme_boost_campus_rwth/brandcolor';
    $title = get_string('brandcolor', 'theme_boost', null, true);
    $description = get_string('brandcolor_desc', 'theme_boost', null, true);
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#00549F');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Variable $brand-succes-color.
    $name = 'theme_boost_campus_rwth/brandsuccesscolor';
    $title = get_string('brandsuccesscolorsetting', 'theme_boost_campus', null, true);
    $description = get_string('brandsuccesscolorsetting_desc', 'theme_boost_campus', null, true);
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#42811D');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Variable $brand-info-color.
    $name = 'theme_boost_campus_rwth/brandinfocolor';
    $title = get_string('brandinfocolorsetting', 'theme_boost_campus', null, true);
    $description = get_string('brandinfocolorsetting_desc', 'theme_boost_campus', null, true);
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#000000');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Variable $brand-warning-color.
    $name = 'theme_boost_campus_rwth/brandwarningcolor';
    $title = get_string('brandwarningcolorsetting', 'theme_boost_campus', null, true);
    $description = get_string('brandwarningcolorsetting_desc', 'theme_boost_campus', null, true);
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#F6A800');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Variable $brand-warning-color.
    $name = 'theme_boost_campus_rwth/branddangercolor';
    $title = get_string('branddangercolorsetting', 'theme_boost_campus', null, true);
    $description = get_string('branddangercolorsetting_desc', 'theme_boost_campus', null, true);
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#CC071E');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Add tab to settings page.
    $settings->add($page);


    // Create advanced settings tab.
    $page = new admin_settingpage('theme_boost_campus_rwth_advanced', get_string('advancedsettings', 'theme_boost', null, true));

    // Raw SCSS to include before the content.
    $name = 'theme_boost_campus_rwth/scsspre';
    $title = get_string('rawscsspre', 'theme_boost', null, true);
    $description = get_string('rawscsspre_desc', 'theme_boost', null, true);
    $setting = new admin_setting_configtextarea($name, $title, $description, '', PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Raw SCSS to include after the content.
    $name = 'theme_boost_campus_rwth/scss';
    $title = get_string('rawscss', 'theme_boost', null, true);
    $description = get_string('rawscss_desc', 'theme_boost', null, true);
    $setting = new admin_setting_configtextarea($name, $title, $description, '', PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Add tab to settings page.
    $settings->add($page);


    // Create front page layout settings tab.
    $page = new admin_settingpage('theme_boost_campus_rwth_frontpage', get_string('frontpage_settings', 'theme_boost_campus_rwth', null, true));

    $name = 'theme_boost_campus_rwth/frontpage_backgroundimage_copyright';
    $title = get_string('frontpage_backgroundimage_copyright_setting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('frontpage_backgroundimage_copyright_setting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_confightmleditor($name, $title, $description, '<p>Martin Braun</p>');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Add tab to settings page.
    $settings->add($page);


    // Create login page layout settings tab.
    $page = new admin_settingpage('theme_boost_campus_rwth_loginpage', get_string('loginpage_settings', 'theme_boost_campus_rwth', null, true));

    // Login page background setting.
    // TODO Reactivate this setting here
    // $name = 'theme_boost_campus_rwth/loginpage_backgroundimage';
    // $title = get_string('loginbackgroundimagesetting', 'theme_boost_campus', null, true);
    // $description = get_string('loginbackgroundimagesetting_desc', 'theme_boost_campus', null, true);
    // $setting = new admin_setting_configstoredfile($name, $title, $description, 'loginbackgroundimage', 0,
    //     array('maxfiles' => 10, 'accepted_types' => 'web_image'));
    // $setting->set_updatedcallback('theme_reset_all_caches');
    // $page->add($setting);

    $name = 'theme_boost_campus_rwth/loginpage_backgroundimage_copyright';
    $title = get_string('loginpage_backgroundimage_copyright_setting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('loginpage_backgroundimage_copyright_setting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_confightmleditor($name, $title, $description, '<p>Peter Winandy</p>');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    $name = 'theme_boost_campus_rwth/loginpage_redirect_to_frontpage';
    $title = get_string('loginpage_redirect_to_frontpage_setting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('loginpage_redirect_to_frontpage_setting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configcheckbox($name, $title, $description, '1');
    $page->add($setting);

    // Add tab to settings page.
    $settings->add($page);

    // Create info banner settings tab.
    $page = new admin_settingpage('theme_boost_campus_rwth_infobanner', get_string('infobannersettings',
            'theme_boost_campus_rwth', null, true));

    // Settings title to group perpetual information banner settings together with a common heading and description.
    $name = 'theme_boost_campus_rwth/perpetualinfobannerheading';
    $title = get_string('perpetualinfobannerheadingsetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpetualinfobannerheadingsetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_heading($name, $title, $description);
    $page->add($setting);

    // Activate perpetual information banner.
    $name = 'theme_boost_campus_rwth/perpibenable';
    $title = get_string('perpibenablesetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpibenablesetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $page->add($setting);

    // Perpetual information banner content.
    $name = 'theme_boost_campus_rwth/perpibcontent';
    $title = get_string('perpibcontent', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpibcontent_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_confightmleditor($name, $title, $description, '');
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/perpibcontent',
            'theme_boost_campus_rwth/perpibenable', 'notchecked');

    // Select pages on which the perpetual information banner should be shown.
    $name = 'theme_boost_campus_rwth/perpibshowonpages';
    $title = get_string('perpibshowonpagessetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpibshowonpagessetting_desc', 'theme_boost_campus_rwth', null, true);
    $perpibshowonpageoptions = [
            // Don't use string lazy loading (= false) because the string will be directly used and would produce a
            // PHP warning otherwise.
            'mydashboard' => get_string('myhome', 'core', null, false),
            'course' => get_string('course', 'core', null, false),
            'login' => get_string('login_page', 'theme_boost_campus_rwth', null, false)
    ];
    $setting = new admin_setting_configmultiselect($name, $title, $description,
            array($perpibshowonpageoptions['mydashboard']), $perpibshowonpageoptions);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/perpibshowonpages',
            'theme_boost_campus_rwth/perpibenable', 'notchecked');

    // Select the bootstrap class that should be used for the perpetual info banner.
    $name = 'theme_boost_campus_rwth/perpibcss';
    $title = get_string('perpibcsssetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpibcsssetting_desc', 'theme_boost_campus_rwth', null, true);
    $perpibcssoptions = [
            // Don't use string lazy loading (= false) because the string will be directly used and would produce a
            // PHP warning otherwise.
            'primary' => get_string('bootstrapprimarycolor', 'theme_boost_campus_rwth', null, false),
            'secondary' => get_string('bootstrapsecondarycolor', 'theme_boost_campus_rwth', null, false),
            'success' => get_string('bootstrapsuccesscolor', 'theme_boost_campus_rwth', null, false),
            'danger' => get_string('bootstrapdangercolor', 'theme_boost_campus_rwth', null, false),
            'warning' => get_string('bootstrapwarningcolor', 'theme_boost_campus_rwth', null, false),
            'info' => get_string('bootstrapinfocolor', 'theme_boost_campus_rwth', null, false),
            'light' => get_string('bootstraplightcolor', 'theme_boost_campus_rwth', null, false),
            'dark' => get_string('bootstrapdarkcolor', 'theme_boost_campus_rwth', null, false)
    ];
    $setting = new admin_setting_configselect($name, $title, $description, $perpibcssoptions['primary'],
            $perpibcssoptions);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/perpibcss',
            'theme_boost_campus_rwth/perpibenable', 'notchecked');

    // Perpetual information banner dismissible.
    $name = 'theme_boost_campus_rwth/perpibdismiss';
    $title = get_string('perpibdismisssetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpibdismisssetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/perpibdismiss',
            'theme_boost_campus_rwth/perpibenable', 'notchecked');

    // Perpetual information banner show confirmation dialogue when dismissing.
    $name = 'theme_boost_campus_rwth/perpibconfirm';
    $title = get_string('perpibconfirmsetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpibconfirmsetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/perpibconfirm',
            'theme_boost_campus_rwth/perpibenable', 'notchecked');
    $settings->hide_if('theme_boost_campus_rwth/perpibconfirm',
            'theme_boost_campus_rwth/perpibdismiss', 'notchecked');

    // Reset the user preference for all users.
    $name = 'theme_boost_campus_rwth/perpibresetvisibility';
    $title = get_string('perpetualinfobannerresetvisiblitysetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('perpetualinfobannerresetvisiblitysetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_boost_campus_rwth_infobanner_reset_visibility');
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/perpibresetvisibility',
            'theme_boost_campus_rwth/perpibenable', 'notchecked');
    $settings->hide_if('theme_boost_campus_rwth/perpibresetvisibility',
            'theme_boost_campus_rwth/perpibdismiss', 'notchecked');

    // Settings title to group time controlled information banner settings together with a common heading and description.
    $name = 'theme_boost_campus_rwth/timedinfobannerheading';
    $title = get_string('timedinfobannerheadingsetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('timedinfobannerheadingsetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_heading($name, $title, $description);
    $page->add($setting);

    // Activate time controlled information banner.
    $name = 'theme_boost_campus_rwth/timedibenable';
    $title = get_string('timedibenablesetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('timedibenablesetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $page->add($setting);

    // Time controlled information banner content.
    $name = 'theme_boost_campus_rwth/timedibcontent';
    $title = get_string('timedibcontent', 'theme_boost_campus_rwth', null, true);
    $description = get_string('timedibcontent_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_confightmleditor($name, $title, $description, '');
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/timedibcontent',
            'theme_boost_campus_rwth/timedibenable', 'notchecked');

    // Select pages on which the time controlled information banner should be shown.
    $name = 'theme_boost_campus_rwth/timedibshowonpages';
    $title = get_string('timedibshowonpagessetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('timedibshowonpagessetting_desc', 'theme_boost_campus_rwth', null, true);
    $timedibpageoptions = [
        // Don't use string lazy loading (= false) because the string will be directly used and would produce a
        // PHP warning otherwise.
            'mydashboard' => get_string('myhome', 'core', null, false),
            'course' => get_string('course', 'core', null, false),
            'login' => get_string('login_page', 'theme_boost_campus_rwth', null, false)
    ];
    $setting = new admin_setting_configmultiselect($name, $title, $description,
            array($timedibpageoptions['mydashboard']), $timedibpageoptions);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/timedibshowonpages',
            'theme_boost_campus_rwth/timedibenable', 'notchecked');

    // Select the bootstrap class that should be used for the perpetual info banner.
    $name = 'theme_boost_campus_rwth/timedibcss';
    $title = get_string('timedibcsssetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('timedibcsssetting_desc', 'theme_boost_campus_rwth', null, true);
    $timedibcssoptions = [
        // Don't use string lazy loading (= false) because the string will be directly used and would produce a
        // PHP warning otherwise.
            'primary' => get_string('bootstrapprimarycolor', 'theme_boost_campus_rwth', null, false),
            'secondary' => get_string('bootstrapsecondarycolor', 'theme_boost_campus_rwth', null, false),
            'success' => get_string('bootstrapsuccesscolor', 'theme_boost_campus_rwth', null, false),
            'danger' => get_string('bootstrapdangercolor', 'theme_boost_campus_rwth', null, false),
            'warning' => get_string('bootstrapwarningcolor', 'theme_boost_campus_rwth', null, false),
            'info' => get_string('bootstrapinfocolor', 'theme_boost_campus_rwth', null, false),
            'light' => get_string('bootstraplightcolor', 'theme_boost_campus_rwth', null, false),
            'dark' => get_string('bootstrapdarkcolor', 'theme_boost_campus_rwth', null, false)
    ];
    $setting = new admin_setting_configselect($name, $title, $description, $timedibcssoptions['primary'],
            $timedibcssoptions);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/timedibcss',
            'theme_boost_campus_rwth/timedibenable', 'notchecked');

    // This will check for the desired date time format YYYY-MM-DD HH:MM:SS
    $timeregex = '/(20[0-9]{2}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\s([0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9])|^$/';

    // Start time for controlled information banner.
    $name = 'theme_boost_campus_rwth/timedibstart';
    $title = get_string('timedibstartsetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('timedibstartsetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configtext($name, $title, $description, '', $timeregex);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/timedibstart',
            'theme_boost_campus_rwth/timedibenable', 'notchecked');

    // End time for controlled information banner.
    $name = 'theme_boost_campus_rwth/timedibend';
    $title = get_string('timedibendsetting', 'theme_boost_campus_rwth', null, true);
    $description = get_string('timedibendsetting_desc', 'theme_boost_campus_rwth', null, true);
    $setting = new admin_setting_configtext($name, $title, $description, '', $timeregex);
    $page->add($setting);
    $settings->hide_if('theme_boost_campus_rwth/timedibend',
            'theme_boost_campus_rwth/timedibenable', 'notchecked');

    // Add tab to settings page.
    $settings->add($page);
}
