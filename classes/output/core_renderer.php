<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2017 Kathrin Osswald, Ulm University kathrin.osswald@uni-ulm.de
 *            2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 *            2018 Steffen Schaffert, RWTH Aachen University <schaffert@itc.rwth-aachen.de>
 *            copyright based on code from theme_boost by Bas Brands
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_boost_campus_rwth\output;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir. '/filestorage/file_storage.php');
require_once($CFG->libdir. '/grouplib.php');
require_once($CFG->libdir. '/outputlib.php');
require_once($CFG->dirroot. '/course/lib.php');

use stdClass;

use html_writer;
use moodle_url;
use moodle_page;
use single_button;
use context_course;
use theme_config;

use theme_boost_campus_rwth\plugin_helper;

defined('MOODLE_INTERNAL') || die;


/**
 * Extending the core_renderer interface.
 *
 * @copyright 2017 Kathrin Osswald, Ulm University kathrin.osswald@uni-ulm.de
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package theme_boost_campus_rwth
 * @category output
 */
class core_renderer extends \theme_boost_campus\output\core_renderer {

    protected $boostcampus;

    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);
        $this->boostcampus = theme_config::load('boost_campus');
    }

    public function favicon() {
        return $this->image_url('Favicon', 'theme_boost_campus_rwth');
    }

    public function edit_button(moodle_url $url) {
        // Print the edit on / off button in course header regardless of the parent theme settings.
        return \core_renderer::edit_button($url);
    }

    public function body_attributes($additionalclasses = array()) {
        global $PAGE, $CFG;

        if (!is_array($additionalclasses)) {
            $additionalclasses = explode(' ', $additionalclasses);
        }

        // MODIFICATION START.
        // Only add classes for the login page.
        if ($PAGE->bodyid == 'page-theme-boost_campus_rwth-login') {
            $additionalclasses[] = 'loginbackgroundimage';
            $additionalclasses[] = 'loginbackgroundimage1'; // Always show first (and only) login background.
        }
        // MODIFICATION END.

        return ' id="'. $this->body_id().'" class="'.$this->body_css_classes($additionalclasses).'"';
    }

    /**
     * Override to display switched role information beneath the course header instead of the user menu.
     * We change this because the switch role function is course related and therefore it should be placed in the course context.
     *
     * Wrapper for header elements.
     *
     * @return string HTML to display the main header.
     */
    public function full_header() {
        global $USER, $COURSE, $CFG;

        if ($this->page->include_region_main_settings_in_header_actions() && !$this->page->blocks->is_block_present('settings')) {
            // Only include the region main settings if the page has requested it and it doesn't already have
            // the settings block on it. The region main settings are included in the settings block and
            // duplicating the content causes behat failures.
            $this->page->add_header_action(html_writer::div(
                    $this->region_main_settings_menu(),
                    'd-print-none',
                    ['id' => 'region-main-settings-menu']
            ));
        }

        $header = new stdClass();
        // Show the context header settings menu on all pages except for the profile page as we replace
        // it with an edit button there and if we are not on the content bank view page (contentbank/view.php)
        // as this page only adds header actions.
        if ($this->page->pagelayout != 'mypublic' && $this->page->bodyid != 'page-contentbank') {
            $header->settingsmenu = $this->context_header_settings_menu();
            }
        $header->contextheader = $this->context_header();
        $header->hasnavbar = empty($this->page->layout_options['nonavbar']);
        $header->navbar = $this->navbar();

        // Show the page heading button on all pages except for the profile page.
        // There we replace it with an edit profile button.
        if ($this->page->pagelayout != 'mypublic') {
            $header->pageheadingbutton = $this->page_heading_button();
        } else {
            // Get the id of the user for whom the profile page is shown.
            $userid = optional_param('id', $USER->id, PARAM_INT);
            // Check if the shown and the operating user are identical.
            $currentuser = $USER->id == $userid;
            if (($currentuser || is_siteadmin($USER)) &&
                    has_capability('moodle/user:update', \context_system::instance())) {
                $url = new moodle_url('/user/editadvanced.php', array('id'       => $userid, 'course' => $COURSE->id,
                                                                          'returnto' => 'profile'));
                $header->pageheadingbutton = $this->single_button($url, get_string('editmyprofile', 'core'));
            } else if ((has_capability('moodle/user:editprofile', \context_user::instance($userid)) &&
                            !is_siteadmin($USER)) || ($currentuser &&
                            has_capability('moodle/user:editownprofile', \context_system::instance()))) {
                $url = new moodle_url('/user/edit.php', array('id'       => $userid, 'course' => $COURSE->id,
                                                                  'returnto' => 'profile'));
                $header->pageheadingbutton = $this->single_button($url, get_string('editmyprofile', 'core'));
            }
        }
        $header->courseheader = $this->course_header();
        $header->headeractions = $this->page->get_header_actions();
        // MODIFICATION START:
        // Display a course logo if one is configured.
        $header->courselogo = $this->render_course_logo();
        // MODIFICATION END.

        // Change this to add the result in the html variable to be able to add further features below the header.
        // Render from the own header template if we are not on the content bank view page (contentbank/view.php).
        if ($this->page->bodyid == 'page-contentbank') {
            $html = $this->render_from_template('core/full_header', $header);
        } else {
            $html = $this->render_from_template('theme_boost_campus/full_header', $header);
        }

        // MODIFICATION START
        // The visibility of the course is hidden and a hint for the visibility will be shown,
        // regardless of the parent theme settings.
        if ($COURSE->visible == false
                && $this->page->has_set_url()
                && $this->page->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE)) {
        // MODIFICATION END
            $html .= html_writer::start_tag('div', array('class' => 'course-hidden-infobox alert alert-warning'));
            $html .= html_writer::tag('i', null, array('class' => 'fa fa-exclamation-circle fa-3x fa-pull-left'));
            $html .= get_string('showhintcoursehiddengeneral', 'theme_boost_campus_rwth', $COURSE->id);

            $pluginhelper = plugin_helper::instance();
            // If the user has the capability to change the course settings, an additional link to the course settings is shown.
            $capname = $pluginhelper->get_capability_name_override('moodle/course:update');
            if (has_capability($capname, context_course::instance($COURSE->id))) {
                $filepath = $pluginhelper->get_file_path_override('/course/edit.php');
                $html .= html_writer::tag('div', get_string('showhintcoursehiddensettingslink',
                    'theme_boost_campus', array('url' => $CFG->wwwroot.$filepath.'?id='. $COURSE->id)));
            }
            $html .= html_writer::end_tag('div');
        }

        // MODIFICATION START
        // A hint for users that view the course with guest access is shown,
        // regardless of the parent theme settings.
        if (is_guest(\context_course::instance($COURSE->id), $USER->id)
                && $this->page->has_set_url()
                && $this->page->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE)) {
        // MODIFICATION END
            $html .= html_writer::start_tag('div', array('class' => 'course-guestaccess-infobox alert alert-warning'));
            $html .= html_writer::tag('i', null, array('class' => 'fa fa-exclamation-circle fa-3x fa-pull-left'));
            $html .= get_string('showhintcourseguestaccessgeneral', 'theme_boost_campus',
                array('role' => role_get_name(get_guest_role())));
            $html .= theme_boost_campus_get_course_guest_access_hint($COURSE->id);
            $html .= html_writer::end_tag('div');
        }

        // MODIFICATION START.
        // Show this regardless of the parent theme settings.
        // MODIFICATION END.
        // Check if user is logged in.
        // If not, adding this section would make no sense and, even worse,
        // user_get_user_navigation_info() will throw an exception due to the missing user object.
        if (isloggedin()) {
            $opts = \user_get_user_navigation_info($USER, $this->page);
            // Role is switched.
            if (!empty($opts->metadata['asotherrole'])) {
                // Get the role name switched to.
                $role = $opts->metadata['rolename'];
                // Get the URL to switch back (normal role).
                $url = new moodle_url('/course/switchrole.php',
                                        array('id'        => $COURSE->id, 'sesskey' => sesskey(), 'switchrole' => 0,
                                            'returnurl' => $this->page->url->out_as_local_url(false)));
                $html .= html_writer::start_tag('div', array('class' => 'switched-role-infobox alert alert-info'));
                $html .= html_writer::tag('i', null, array('class' => 'fa fa-user-circle fa-3x fa-pull-left'));
                $html .= html_writer::start_tag('div');
                $html .= get_string('switchedroleto', 'theme_boost_campus');
                // Give this a span to be able to address via CSS.
                $html .= html_writer::tag('span', $role, array('class' => 'switched-role'));
                $html .= html_writer::end_tag('div');
                // Return to normal role link.
                $html .= html_writer::start_tag('div');
                $html .= html_writer::tag('a', get_string('switchrolereturn', 'core'),
                                            array('class' => 'switched-role-backlink', 'href' => $url));
                $html .= html_writer::end_tag('div'); // Return to normal role link: end div.
                $html .= html_writer::end_tag('div');
            }
        }
        return $html;
    }

    /**
     * Override to display user's name even if user_can_view_profile() == false.
     *
     * This override is only necessary as long as this Moodle issue is not resolved:
     * https://tracker.moodle.org/browse/MDL-68669
     */
    public function context_header($headerinfo = null, $headinglevel = 1) {
        global $DB, $USER, $CFG, $SITE;
        require_once($CFG->dirroot . '/user/lib.php');
        $context = $this->page->context;
        $heading = null;
        $imagedata = null;
        $subheader = null;
        $userbuttons = null;

        if ($this->should_display_main_logo($headinglevel)) {
            $sitename = format_string($SITE->fullname, true, array('context' => context_course::instance(SITEID)));
            return html_writer::div(html_writer::empty_tag('img', [
                    'src' => $this->get_logo_url(null, 150), 'alt' => $sitename, 'class' => 'img-fluid']), 'logo');
        }

        // Make sure to use the heading if it has been set.
        if (isset($headerinfo['heading'])) {
            $heading = $headerinfo['heading'];
        }

        // The user context currently has images and buttons. Other contexts may follow.
        if (isset($headerinfo['user']) || $context->contextlevel == CONTEXT_USER) {
            if (isset($headerinfo['user'])) {
                $user = $headerinfo['user'];
            } else {
                // Look up the user information if it is not supplied.
                $user = $DB->get_record('user', array('id' => $context->instanceid));
            }

            // If the user context is set, then use that for capability checks.
            if (isset($headerinfo['usercontext'])) {
                $context = $headerinfo['usercontext'];
            }

            // Only provide user information if the user is the current user, or a user which the current user can view.
            // When checking user_can_view_profile(), either:
            // If the page context is course, check the course context (from the page object) or;
            // If page context is NOT course, then check across all courses.
            $course = ($this->page->context->contextlevel == CONTEXT_COURSE) ? $this->page->course : null;

            if (user_can_view_profile($user, $course)) {
                // Use the user's full name if the heading isn't set.
                if (!isset($heading)) {
                    $heading = fullname($user);
                }

                $imagedata = $this->user_picture($user, array('size' => 100));

                // Check to see if we should be displaying a message button.
                if (!empty($CFG->messaging) && has_capability('moodle/site:sendmessage', $context)) {
                    $userbuttons = array(
                        'messages' => array(
                            'buttontype' => 'message',
                            'title' => get_string('message', 'message'),
                            'url' => new moodle_url('/message/index.php', array('id' => $user->id)),
                            'image' => 'message',
                            'linkattributes' => \core_message\helper::messageuser_link_params($user->id),
                            'page' => $this->page
                        )
                    );

                    if ($USER->id != $user->id) {
                        $iscontact = \core_message\api::is_contact($USER->id, $user->id);
                        $contacttitle = $iscontact ? 'removefromyourcontacts' : 'addtoyourcontacts';
                        $contacturlaction = $iscontact ? 'removecontact' : 'addcontact';
                        $contactimage = $iscontact ? 'removecontact' : 'addcontact';
                        $userbuttons['togglecontact'] = array(
                                'buttontype' => 'togglecontact',
                                'title' => get_string($contacttitle, 'message'),
                                'url' => new moodle_url('/message/index.php', array(
                                        'user1' => $USER->id,
                                        'user2' => $user->id,
                                        $contacturlaction => $user->id,
                                        'sesskey' => sesskey())
                                ),
                                'image' => $contactimage,
                                'linkattributes' => \core_message\helper::togglecontact_link_params($user, $iscontact),
                                'page' => $this->page
                            );
                    }

                    $this->page->requires->string_for_js('changesmadereallygoaway', 'moodle');
                }
            } else {
                // MODIFICATION START:
                // Display user's name even if we can't view his profile.
                $heading = preg_replace('~<a [^>]+>(.*?)</a>~', '$1', $heading);
                // MODIFICATION END.
            }
        }

        $contextheader = new \context_header($heading, $headinglevel, $imagedata, $userbuttons);
        return $this->render_context_header($contextheader);
    }

    public function render_course_logo() {
        global $COURSE, $CFG;
        // copied from https://moodle.org/mod/forum/discuss.php?d=321604#p1291375
        if ($COURSE->id == SITEID || empty($CFG->courseoverviewfileslimit)) {
            return '';
        }
        $fs = get_file_storage();
        $context = context_course::instance($COURSE->id);
        $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', false, 'filename', false);
        if (!count($files)) {
            return '';
        }
        $overviewfilesoptions = course_overviewfiles_options($COURSE->id);
        $acceptedtypes = $overviewfilesoptions['accepted_types'];
        if ($acceptedtypes !== '*') {
            // Filter only files with allowed extensions.
            require_once($CFG->libdir. '/filelib.php');
            foreach ($files as $key => $file) {
                if (!file_extension_in_typegroup($file->get_filename(), $acceptedtypes)) {
                    unset($files[$key]);
                }
            }
        }
        if (count($files) > $CFG->courseoverviewfileslimit) {
            // Return no more than $CFG->courseoverviewfileslimit files.
            $files = array_slice($files, 0, $CFG->courseoverviewfileslimit, true);
        }

        // Display course overview files.
        $courseimage = '';
        foreach ($files as $file) {
            $isimage = $file->is_valid_image();
            if (!$isimage) {
                continue;
            }
            $courseimage = file_encode_url("$CFG->wwwroot/pluginfile.php",
                '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
        }
        if (empty($courseimage)) {
            return '';
        }
        return html_writer::img($courseimage, 'course logo', ['height' => '45', 'class' => 'course-logo']);
    }

    /**
     * Override to revert theme_boost_campus login template changes
     * Renders the login form.
     *
     * @param \core_auth\output\login $form The renderable.
     * @return string
     */
    public function render_login(\core_auth\output\login $form) {
        global $SITE;

        $context = $form->export_for_template($this);

        // Override because rendering is not supported in template yet.
        $context->cookieshelpiconformatted = $this->help_icon('cookiesenabled');
        $context->errorformatted = $this->error_text($context->error);
        $url = $this->get_logo_url();
        if ($url) {
            $url = $url->out(false);
        }
        $context->logourl = $url;
        $context->sitename = format_string($SITE->fullname, true,
            ['context' => context_course::instance(SITEID), "escape" => false]);

        // TODO Always show footer if content was configured (links or a copyright notice, see below)

        return $this->render_from_template('core/loginform', $context);
    }

    /**
     * Disable setting "forcelogin" temporarily to allow blocks to be rendered on:
     *   - login page
     *   - static pages
     *
     * @return string HTML fragment.
     */
    public function standard_head_html() {
        global $PAGE, $CFG;
        $onloginpage = $PAGE->pagelayout == 'login';
        $onstaticpage = $PAGE->pagetype == 'local-staticpage-view';

        // Disable forcelogin.
        if ($onloginpage || $onstaticpage) {
            $backup = $CFG->forcelogin;
            $CFG->forcelogin = '0';
        }

        $output = parent::standard_head_html();

        // Reset forcelogin.
        if ($onloginpage) {
            $CFG->forcelogin = $backup;
        }

        return $output;
    }

    /**
     * Prevent site logo from being restricted to 100px width.
     */
    public function get_compact_logo_url($maxwidth = 100, $maxheight = 100) {
        return parent::get_compact_logo_url(0, 70);
    }
}
 
class core_group_renderer extends \core_group\output\renderer {
 
    /**
     * Defer to template.
     *
     * @param index_page $page
     * @return string
     */
    public function render_index_page(\core_group\output\index_page $page) {
        global $COURSE, $DB, $PAGE, $CFG;

        $notification = '';
        $context = context_course::instance($COURSE->id);
        $capname = plugin_helper::instance()->get_capability_name_override('moodle/course:update');
        if (has_capability($capname, $context)) {

            // Display notification if groups are currently disabled for this course.
            $groupmode = $DB->get_field('course', 'groupmode', array('id' => $COURSE->id), MUST_EXIST);
            if ($groupmode == NOGROUPS) {

                $adminrenderer = $PAGE->get_renderer('core', 'admin');

                $a = new stdClass();
                $pluginhelper = plugin_helper::instance();
                $coursesettings = $pluginhelper->get_file_path_override('/course/edit.php');
                $a->coursesettings = $CFG->wwwroot.$coursesettings.'?id='.$COURSE->id;
                $helppath = page_get_doc_link_path($PAGE);
                $helpurl = new moodle_url(get_docs_url($helppath));
                $a->help = $helpurl->out();

                $notification = $adminrenderer->box(
                        get_string('groupsnotenabled', 'theme_boost_campus_rwth', $a),
                        'generalbox m-b-0 admin alert alert-warning');
            }
        }
        return $notification . parent::render_index_page($page);
    }
}
