<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Allows the local plugin rwth_sd_sync to override the calendar renderer
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_boost_campus_rwth\output;

defined('MOODLE_INTERNAL') || die();

use theme_boost_campus_rwth\plugin_helper;

$sdsyncversion = plugin_helper::instance()->get_installed_plugin_version('local', 'rwth_sd_sync');
if ($sdsyncversion !== null && $sdsyncversion > 2018061200) {
    class core_calendar_renderer extends \local_rwth_sd_sync\output\core_calendar_renderer {
    }
} else {
    class core_calendar_renderer extends \core_calendar_renderer {
    }
}
