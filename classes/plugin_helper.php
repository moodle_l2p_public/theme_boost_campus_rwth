<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class that can e.g. check whether a plugin is installed or which version it has.
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_boost_campus_rwth;

use core_plugin_manager;

defined('MOODLE_INTERNAL') || die();

class plugin_helper {

    /**
     * Direct initiation not allowed, use the factory method {@link self::instance()}
     */
    protected function __construct() {
        $this->pluginmanager = core_plugin_manager::instance();
    }

    /**
     * Sorry, this is singleton
     */
    protected function __clone() {
    }

    /**
     * Factory method for this class
     *
     * @return core_plugin_manager the singleton instance
     */
    public static function instance() {
        if (is_null(static::$singletoninstance)) {
            static::$singletoninstance = new static();
        }
        return static::$singletoninstance;
    }

    /**
     * Returns the original capability name or an overridded one if a certain plugin
     * replaces the capability with one of its own.
     */
    public function get_capability_name_override(string $originalname) {
        if ($originalname == 'moodle/course:update' &&
                $this->is_plugin_installed('local', 'rwth_settings_adjustment')) {
            return 'local/rwth_settings_adjustment:edit_course_settings';
        }
        return $originalname;
    }

    /**
     * Returns the original file path or an overridded one if a certain plugin
     * replaces the file with one of its own.
     */
    public function get_file_path_override(string $originalpath) {
        if ($originalpath == '/course/edit.php' &&
                $this->is_plugin_installed('local', 'rwth_settings_adjustment')) {
            return '/local/rwth_settings_adjustment/admin.php';
        }
        return $originalpath;
    }

    public function is_plugin_installed(string $plugintype, string $pluginname) {
        return self::get_installed_plugin_version($plugintype, $pluginname) !== null;
    }

    public function get_installed_plugin_version(string $plugintype, string $pluginname) {
        if (!array_key_exists($plugintype, $this->installedplugins)) {
            $this->installedplugins[$plugintype] = $this->pluginmanager->get_installed_plugins($plugintype);
        }
        if (!array_key_exists($pluginname, $this->installedplugins[$plugintype])) {
            return null;
        }
        return $this->installedplugins[$plugintype][$pluginname];
    }
	
	/*Puts the code for the floating Edit Button in the header */
	// public function floating_edit_button_helper() {
	// 	global $PAGE, $CFG;
	// 	$bla=new moodle_url($CFG->dirroot . '../');
	// 	$PAGE->requires->js(new moodle_url($CFG->dirroot . '../'); 
	// }

    protected static $singletoninstance;
    protected $pluginmanager = null;
    /** @var array plugintype => array of installed plugins */
    protected $installedplugins = array();

}
