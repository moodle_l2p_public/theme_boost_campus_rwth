<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Library
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2017 Kathrin Osswald, Ulm University <kathrin.osswald@uni-ulm.de>
 *            2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Returns the main SCSS content.
 *
 * @param theme_config $theme The theme config object.
 * @return string
 */
function theme_boost_campus_rwth_get_main_scss_content($theme) {
    global $CFG;

    $scss = '';
    $filename = !empty($theme->settings->preset) ? $theme->settings->preset : null;
    $fs = get_file_storage();

    $context = context_system::instance();
    if ($filename == 'default.scss') {
        // We still load the default preset files directly from the boost theme. No sense in duplicating them.
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/default.scss');
    } else if ($filename == 'plain.scss') {
        // We still load the default preset files directly from the boost theme. No sense in duplicating them.
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/plain.scss');

    } else if ($filename && ($presetfile = $fs->get_file($context->id, 'theme_boost_campus_rwth', 'preset', 0, '/', $filename))) {
        // This preset file was fetched from the file area for theme_boost_campus_rwth and not theme_boost (see the line above).
        $scss .= $presetfile->get_content();
    } else {
        // Safety fallback - maybe new installs etc.
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/default.scss');
    }

    // Load additional scss for this theme.
    $rwthscss = file_get_contents($CFG->dirroot . '/theme/boost_campus_rwth/scss/index.scss');

    // Pre CSS - this is loaded AFTER any prescss from the setting but before the main scss.
    $pre = file_get_contents($CFG->dirroot . '/theme/boost_campus_rwth/scss/pre.scss');
    $preboostcampus = file_get_contents($CFG->dirroot . '/theme/boost_campus/scss/pre.scss');
    // Post CSS - this is loaded AFTER the main scss but before the extra scss from the setting.
    $post = file_get_contents($CFG->dirroot . '/theme/boost_campus_rwth/scss/post.scss');
    $postboostcampus = file_get_contents($CFG->dirroot . '/theme/boost_campus/scss/post.scss');

    // Combine them together.
    return $preboostcampus . "\n" . $pre . "\n" . $scss . "\n" . $rwthscss . "\n" . $postboostcampus ."\n" . $post;
}

/**
 * Override to add CSS values from settings to pre scss file.
 *
 * Get SCSS to prepend.
 *
 * @param theme_config $theme The theme config object.
 * @return array
 */
function theme_boost_campus_rwth_get_pre_scss($theme) {
    global $CFG;
    // Add the variables from the theme Boost Campus settings.
    // We need to work with stdclass here because the function expects the parameter to have a certain data structure.
    // We rebuild this data structure here.
    $boostcampusconfig = new stdclass();
    $boostcampusconfig->settings = get_config('theme_boost_campus');
    $boostcampusprescss = theme_boost_campus_get_pre_scss($boostcampusconfig);

    require_once($CFG->dirroot . '/theme/boost_campus_rwth/locallib.php');

    $scss = '';
    $configurable = [
        // Config key => [variableName, ...].
        'brandcolor' => ['primary'],
        // Add own variables.
        'brandsuccesscolor' => ['success'],
        'brandinfocolor' => ['info'],
        'brandwarningcolor' => ['warning'],
        'branddangercolor' => ['danger'],
    ];

    // Prepend variables first.
    foreach ($configurable as $configkey => $targets) {
        $value = isset($theme->settings->{$configkey}) ? $theme->settings->{$configkey} : null;
        if (empty($value)) {
            continue;
        }
        array_map(function($target) use (&$scss, $value) {
            $scss .= '$' . $target . ': ' . $value . ";\n";
        }, (array) $targets);
    }

    // Prepend pre-scss.
    if (!empty($theme->settings->scsspre)) {
        $scss .= $theme->settings->scsspre;
    }

    // Add login background.
    $scss .= theme_boost_campus_rwth_get_loginbackgroundimage_scss();
    $scss .= theme_boost_campus_rwth_get_frontpagebackgroundimage_scss();

    return $boostcampusprescss . $scss;
}

function theme_boost_campus_rwth_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    return theme_boost_campus_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, $options);
}

function theme_boost_campus_rwth_before_footer() {
    global $PAGE, $CFG;
    $js = file_get_contents($CFG->dirroot.'/theme/boost_campus_rwth/javascript/clamp.min.js');
    $js .= <<<EOS
    // forEach method, could be shipped as part of an Object Literal/Module
var forEach = function (array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
    callback.call(scope, i, array[i]); // passes back stuff we need
    }
};

var myNodeList = document.querySelectorAll('.line-clamp-2');
forEach(myNodeList, function (index, value) {
    \$clamp(value, {clamp: 2});
});
var myNodeList = document.querySelectorAll('.line-clamp-3');
forEach(myNodeList, function (index, value) {
    \$clamp(value, {clamp: 3});
});
var myNodeList = document.querySelectorAll('.line-clamp-4');
forEach(myNodeList, function (index, value) {
    \$clamp(value, {clamp: 4});
});
var myNodeList = document.querySelectorAll('.line-clamp-5');
forEach(myNodeList, function (index, value) {
    \$clamp(value, {clamp: 5});
});
EOS;
    $PAGE->requires->js_amd_inline($js);
}

/**
 * If setting is updated, use this callback to reset the theme_boost_campus_infobanner_dismissed user preferences.
 */
function theme_boost_campus_rwth_infobanner_reset_visibility() {
    global $DB;

    if (get_config('theme_boost_campus_rwth', 'perpibresetvisibility') == 1) {
        // Get all users that have dismissed the info banner once and therefore the user preference.
        $whereclause = 'name = :name AND value = :value';
        $params = ['name' => 'theme_boost_campus_infobanner_dismissed', 'value' => '1'];
        $users = $DB->get_records_select('user_preferences', $whereclause, $params, '', 'userid');

        // Initialize variable for feedback messages.
        $somethingwentwrong = false;
        // Store coding exception.
        $codingexception[] = array();

        foreach ($users as $user) {
            try {
                unset_user_preference('theme_boost_campus_infobanner_dismissed', $user->userid);
            } catch (coding_exception $e) {
                $somethingwentwrong = true;
                $codingexception['message'] = $e->getMessage();
                $codingexception['stacktrace'] = $e->getTraceAsString();
            }
        }

        if (!$somethingwentwrong) {
            \core\notification::success(get_string('resetperpetualinfobannersuccess', 'theme_boost_campus'));
        } else {
            \core\notification::error(get_string('resetperpetualinfobannervisibilityerror',
                    'theme_boost_campus', $codingexception));
        }

        // Reset the checkbox.
        set_config('perpibresetvisibility', 0, 'theme_boost_campus_rwth');
    }
}