<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Language pack
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 *            2018 Steffen Schaffert, RWTH Aachen University <schaffert@itc.rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// GENERAL.
$string['pluginname'] = 'Boost Campus RWTH';
$string['choosereadme'] = 'Boost Campus RWTH ist das Standard-Theme der RWTH Aachen. Es ist ein Child Theme von Boost Campus.';
$string['privacy:metadata'] = 'Das Theme Boost Campus RWTH speichert keine persönlichen Daten über Nutzer.';

// SETTINGS.
$string['configtitle'] = 'Boost Campus RWTH Einstellungen';
$string['frontpage_settings'] = 'Einstellungen Startseite';
$string['frontpage_backgroundimage_copyright_setting'] = 'Hinweis zum Urheber des Hintergrundbildes';
$string['frontpage_backgroundimage_copyright_setting_desc'] = 'Fügt einen Hinweis zum Urheber des Hintergrundbildes für die Startseite hinzu. Zu beachten: Aktuell ist das Hintergrundbild hartkodiert und kann nicht geändert werden';
$string['loginpage_settings'] = 'Einstellungen Login-Seite';
$string['loginpage_backgroundimage_copyright_setting'] = 'Hinweis zum Urheber des Hintergrundbildes';
$string['loginpage_backgroundimage_copyright_setting_desc'] = 'Fügt einen Hinweis zum Urheber des Hintergrundbildes für die Login-Seite hinzu. Zu beachten: Aktuell ist das Hintergrundbild hartkodiert und kann nicht geändert werden.';
$string['loginpage_redirect_to_frontpage_setting'] = 'Auf die Startseite weiterleiten';
$string['loginpage_redirect_to_frontpage_setting_desc'] = 'Mit dieser Einstellung wird man von der Anmeldeseite auf die Startseite weitergeleitet. Um die Weiterleitung zu verhindern, muss man an die URL der Anmeldeseite ein ?redirect=0 dran hängen.';

// Info banner settings.
$string['infobannersettings'] = 'Infobanner Einstellungen';

// ...Perpetual information banner.
$string['perpetualinfobannerheadingsetting'] = 'Dauerhafter Informationsbanner';
$string['perpetualinfobannerheadingsetting_desc'] = 'Die folgenden Einstellungen erlauben es, einige wichtige Informationen mit einem prominenten, dauerhaften Banner anzuzeigen.';
$string['perpibenablesetting'] = 'Dauerhaften Infobanner aktivieren';
$string['perpibenablesetting_desc'] = 'Mit dieser Einstellung können Sie entscheiden, ob der dauerhafte Informationsbanner auf den ausgewählten Seiten angezeigt werden soll oder nicht.';
$string['perpibcontent'] = 'Inhalt des dauerhaften Informationsbanners';
$string['perpibcontent_desc'] = 'Geben Sie hier Ihre Information, die Sie im Banner anzeigen lassen möchten, ein.';
$string['perpibshowonpagessetting'] = 'Seitenlayouts, auf denen der Infobanner angezeigt werden soll';
$string['perpibshowonpagessetting_desc'] = 'Mit dieser Einstellung können Sie die Seiten auswählen, auf denen der Infobanner angezeigt werden soll';
$string['perpibcsssetting'] = 'Boostrap CSS Klasse für den dauerhaften Infobanner';
$string['perpibcsssetting_desc'] = 'Mit dieser Einstellung können Sie den Bootstrap Stil auswählen mit dem der dauerhafte Informationsbanner angezeigt werden soll.';
$string['perpibdismisssetting'] = 'Ausblendbarkeit des dauerhaften Infobanners';
$string['perpibdismisssetting_desc'] = 'Mit dieser Einstellung können Sie den Infobanner dauerhaft ausblendbar machen. Wenn der/die Nutzer/in auf das X klickt, erscheint ein ein Bestätigungsdialog und erst  nachdem der/die Nutzer/in diesen bestätigt hat, wird der Banner dieser Person dauerhaft nicht mehr angezeigt.
<br/><br/>Bitte beachten Sie:<br/>Diese Einstellung hat keine Auswirkung auf Infobanner, die auf der Loginseite angezeigt werden. Infobanner auf der Loginseite können nicht dauerhaft verborgen werden, weswegen wir dort die Option diese überhaupt ausblenden zu können gar nicht erst angeboten wird.';
$string['perpibconfirmsetting'] = 'Bestätigungsdialog';
$string['perpibconfirmsetting_desc'] = 'Wenn Sie diese Einstellung aktivieren, wird dem/der Nutzer/in ein Bestätigungsdialog angezeigt, wenn er/sie den Infobanner wegklickt.
<br/>Der dort angezeigte Text ist im String mit dem Namen "closingperpetualinfobanner" gespeichert:
<br/><br/>
Sind Sie sicher, dass Sie diese Information ausblenden möchten? Sobald Sie diese ausgeblendet haben, wird sie nicht erneut erscheinen!
<br/><br/>
Sie können diesen Text über die Sprachanpassung ändern, wenn Sie hier etwas anderes ausgeben möchten.';
$string['perpetualinfobannerresetvisiblitysetting'] = 'Sichtbarkeit des dauerhaften Infobanners zurücksetzen';
$string['perpetualinfobannerresetvisiblitysetting_desc'] = 'Wenn Sie diese Checkbox anhaken, dann werden die von den Nutzern eventuell ausgeblendeten Infobanner wieder angezeigt. Sie können diese Einstellung nutzen, wenn Sie wichtige Änderungen am Inhalt gemacht haben und diese Information allen Nutzern wieder präsentieren möchten. <br/><br/>
Bitte beachten Sie: <br/>
Wenn Sie diese Änderung speichern, werden die Datenbankoperationen zum Zurücksetzen der Sichtbarkeit gestartet und diese Einstellung wird automatisch wieder deaktiviert. Das nächste Mal, wenn Sie diese Einstellung wieder setzen und speichern werden erneut die Datenbankoperationen gestartet.';

// ...Time controlled information banner.
$string['timedinfobannerheadingsetting'] = 'Zeitgesteuerter Infobanner';
$string['timedinfobannerheadingsetting_desc'] = 'Die folgenden Einstellungen ermöglichen es, wichtige Informationen innerhalb eines prominenten, zeitgesteuerten Banners anzuzeigen.';
$string['timedibenablesetting'] = 'Zeitgesteuerten Infobanner aktivieren';
$string['timedibenablesetting_desc'] = 'Mit dieser Einstellung können Sie entscheiden, ob der zeitgesteuerte Informationsbanner auf den ausgewählten Seiten angezeigt werden soll oder nicht.';
$string['timedibcontent'] = 'Inhalt des zeitgesteuerten Informationsbanners';
$string['timedibcontent_desc'] = 'Geben Sie hier Ihre Information, die Sie im zeitgesteuerten Banner anzeigen lassen möchten, ein.';
$string['timedibshowonpagessetting'] = 'Seitenlayouts, auf denen der Infobanner angezeigt werden soll';
$string['timedibshowonpagessetting_desc'] = 'Mit dieser Einstellung können Sie die Seiten auswählen, auf denen der zeitgesteuerte Infobanner angezeigt werden soll.
<br/> Wenn beide Bannerarten auf einem ausgewählten Seitenlayout aktiv sind, dann wird der zeitgesteuerte Infobanner immer über dem dauerhaften Infobanner angezeigt!';
$string['timedibcsssetting'] = 'Boostrap CSS Klasse für den zeitgesteuerten Infobanner';
$string['timedibcsssetting_desc'] = 'Mit dieser Einstellung können Sie den Bootstrap Stil auswählen mit dem der zeitgesteuerte Informationsbanner angezeigt werden soll.';
$string['timedibstartsetting'] = 'Startzeit für den zeitgesteuerten Infobanner';
$string['timedibstartsetting_desc'] = 'Mit dieser Einstellung können Sie definieren, wann der zeitgesteuerte Informationsbanner auf den ausgewählten Seiten angzeigt werden soll.
<br/>Bitte geben Sie ein gültiges Datum in diesem Format an: JJJJ-MM-TT SS:MM:SS. Zu Beispiel: "2020-01-07 08:00:00". Als Zeitzone wird diejenige genommen, die in der Einstellung "Zeitzone" eingestellt ist.
<br/>Wenn Sie diese Einstellung leer lassen, aber ein Datum für das Ende eingegeben haben, dann verhält es sich genau so als ob Sie ein Datum weit in der Vergangenheit eingegeben hätten.';
$string['timedibendsetting'] = 'Endzeit für den zeitgesteuerten Infobanner';
$string['timedibendsetting_desc'] = 'Mit dieser Einstellung können Sie definieren, wann der zeitgesteuerte Informationsbanner auf den ausgewählten Seiten verborgen werden soll.
<br/>Bitte geben Sie ein gültiges Datum in diesem Format an: JJJJ-MM-TT SS:MM:SS. Zu Beispiel: "2020-01-07 08:00:00". Als Zeitzone wird diejenige genommen, die in der Einstellung "Zeitzone" eingestellt ist.
<br/>Wenn Sie diese Einstellung leer lassen, aber ein Datum für den Beginn eingegeben haben, dann wird der Banner nicht von alleine verschwinden, nachdem der Startzeitpunkt erreicht wurde.';

// ADDITIONAL STRINGS (IN ALPHABETICAL ORDER).
$string['bootstrapprimarycolor'] = 'Primary color';
$string['bootstrapsecondarycolor'] = 'Secondary color';
$string['bootstrapsuccesscolor'] = 'Success color';
$string['bootstrapdangercolor'] = 'Danger color';
$string['bootstrapwarningcolor'] = 'Warning color';
$string['bootstrapinfocolor'] = 'Info color';
$string['bootstraplightcolor'] = 'Light color';
$string['bootstrapdarkcolor'] = 'Dark color';
$string['login_page'] = 'Loginseite';

// Group management.
$string['groupsnotenabled'] = 'Sie können Gruppen anlegen, Mitglieder zuweisen und in einzelnen Aktivitäten nutzen. Um Gruppen standardmäßig kursweit in allen Aktivitäten zu nutzen, ändern Sie die <a href="{$a->coursesettings}">Kurseinstellungen</a>. Weitere <a href="{$a->help}">Hilfe zu Gruppen</a>.';

$string['showhintcoursehiddengeneral'] = 'Dieser Kurs ist zur Zeit <strong>verborgen</strong>. Solange der Kurs weiter verborgen ist, können ausschließlich eingeschriebene Manager/innen darauf zugreifen.';
$string['messaging_warning'] = 'Hinweis: Manager/innen können potenziell alles lesen, was in einem Gruppenchat geschrieben wurde, indem sie sich selbst zur Gruppe hinzufügen.';

// Login page strings
$string['cookiesenabled'] = 'Cookies müssen aktiviert sein, um Moodle zu verwenden.';
$string['username'] = 'Benutzername';
$string['usernameemail'] = 'Benutzername oder E-Mail-Adresse';
$string['rememberusername'] = 'Benutzernamen merken';
$string['login'] = 'Anmelden';
$string['separator_or'] = 'oder';
$string['forgotten'] = 'Zugangsdaten vergessen?';

// Regions.
$string['region-footer-left'] = 'Footer (links)';
$string['region-footer-middle'] = 'Footer (mitte)';
$string['region-footer-right'] = 'Footer (rechts)';
$string['region-footer-badge'] = 'Footer (Abzeichen)';
$string['region-side-pre'] = 'Rechts';
$string['region-above-content'] = 'Über Inhalt';
$string['region-login-mobile'] = 'Login (mobil)';
$string['region-login-desktop'] = 'Login (Desktop)';