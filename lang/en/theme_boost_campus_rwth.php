<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus RWTH - Language pack
 *
 * @package   theme_boost_campus_rwth
 * @copyright 2018 Tim Schroeder, RWTH Aachen University <t.schroeder@itc.rwth-aachen.de>
 *            2018 Steffen Schaffert, RWTH Aachen University <schaffert@itc.rwth-aachen.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// GENERAL.
$string['pluginname'] = 'Boost Campus RWTH';
$string['choosereadme'] = 'Boost Campus RWTH is the default theme of RWTH Aachen university. It is a child theme of Boost Campus.';
$string['privacy:metadata'] = 'The Boost Campus RWTH theme does not store any personal data about any user.';

// SETTINGS.
$string['configtitle'] = 'Boost Campus RWTH settings';
$string['frontpage_settings'] = 'Frontpage Settings';
$string['frontpage_backgroundimage_copyright_setting'] = 'Copyright notice for background image';
$string['frontpage_backgroundimage_copyright_setting_desc'] = 'Add a copyright notice concerning the background image on the frontpage. Please note: Currently, the background image is hardcoded in this theme and you can\'t change it';
$string['loginpage_settings'] = 'Login Page Settings';
$string['loginpage_backgroundimage_copyright_setting'] = 'Copyright notice for background image';
$string['loginpage_backgroundimage_copyright_setting_desc'] = 'Add a copyright notice concerning the background image on the login page. Please note: Currently, the background image is hardcoded in this theme and you can\'t change it.';
$string['loginpage_redirect_to_frontpage_setting'] = 'Redirect to frontpage';
$string['loginpage_redirect_to_frontpage_setting_desc'] = 'With this setting turned on you get redirected to the frontpage by default when visiting the login page. To prevent the redirection you need to append ?redirect=0 to the URL of the login page.';

// Info banner settings.
$string['infobannersettings'] = 'Info Banner Settings';

// ...Perpetual information banner.
$string['perpetualinfobannerheadingsetting'] = 'Perpetual information banner';
$string['perpetualinfobannerheadingsetting_desc'] = 'The following settings allow to show some important information within a prominent perpetual banner.';
$string['perpibenablesetting'] = 'Enable perpetual info banner';
$string['perpibenablesetting_desc'] = 'With this checkbox you can decide if the perpetual information banner should be shown or hidden on the selected pages.';
$string['perpibcontent'] = 'Perpetual information banner content';
$string['perpibcontent_desc'] = 'Enter your information which should be shown within the banner here.';
$string['perpibshowonpagessetting'] = 'Page layouts to display the info banner on';
$string['perpibshowonpagessetting_desc'] = 'With this setting you can select the pages on which the perpetual information banner should be displayed.';
$string['perpibcsssetting'] = 'Bootstrap css class for the perpetual info banner';
$string['perpibcsssetting_desc'] = 'With this setting you can select the Bootstrap style with which the perpetual information banner should be displayed.';
$string['perpibdismisssetting'] = 'Perpetual info banner dismissible';
$string['perpibdismisssetting_desc'] = 'With this checkbox you can make the banner dismissible permanently. If the user clicks on the x-button a confirmation dialogue will appear and only after the user confirmed this dialogue the banner will be hidden for this user permanently.
<br/><br/>Please note: <br/> This setting has no effect for the banners shown on the login page. Because banners on the login page cannot be clicked away permanently, we do not offer the possibility to click the banner away at all on the login page.';
$string['perpibconfirmsetting'] = 'Confirmation dialogue';
$string['perpibconfirmsetting_desc'] = 'When you enable this setting you can show a confirmation dialogue to a user when he is dismissing the info banner.
<br/>The text is saved in the string with the name "closingperpetualinfobanner":<br/><br/>
Are you sure you want to dismiss this information? Once done it will not occur again!<br/><br/>
You can override this within your language customization if you need some other text in this dialogue.';
$string['perpetualinfobannerresetvisiblitysetting'] = 'Reset visibility for perpetual info banner';
$string['perpetualinfobannerresetvisiblitysetting_desc'] = 'By enabling this checkbox, the visibility of the individually dismissed perpetual info banners will be set to visible again. You can use this setting if you made important content changes and want to show the info to all users again.<br/><br/>
Please note: <br/>
After saving this option, the database operations for resetting the visibility will be triggered and this checkbox will be unticked again. The next enabling and saving of this feature will trigger the database operations for resetting the visibility again.';

// ...Time controlled information banner.
$string['timedinfobannerheadingsetting'] = 'Time controlled information banner';
$string['timedinfobannerheadingsetting_desc'] = 'The following settings allow to show some important information within a prominent time controlled banner.';
$string['timedibenablesetting'] = 'Enable time controlled info banner';
$string['timedibenablesetting_desc'] = 'With this checkbox you can decide if the time controlled information banner should be shown or hidden on the selected pages.';
$string['timedibcontent'] = 'Time controlled information banner content';
$string['timedibcontent_desc'] = 'Enter your information which should be shown within the time controlled banner here.';
$string['timedibshowonpagessetting'] = 'Page layouts to display the info banner on';
$string['timedibshowonpagessetting_desc'] = 'With this setting you can select the pages on which the time controlled information banner should be displayed.
<br/> If both info banners are active on a selected layout, the time controlled info banner will always appear above the perpetual info banner!';
$string['timedibcsssetting'] = 'Bootstrap css class for the time controlled info banner';
$string['timedibcsssetting_desc'] = 'With this setting you can select the Bootstrap style with which the time controlled information banner should be displayed.';
$string['timedibstartsetting'] = 'Start time for the time controlled info banner';
$string['timedibstartsetting_desc'] = 'With this setting you can define when the time controlled information banner should be displayed on the selected pages.
<br/>Please enter a valid in this format: YYYY-MM-DD HH:MM:SS. For example: "2020-01-01 08:00:00". The time zone will be the time zone you have defined in the setting "Default timezone".
<br/>If you leave this setting empty but entered a date in the for the end, it is the same as if you entered a date far in the past.';
$string['timedibendsetting'] = 'End time for the time controlled info banner';
$string['timedibendsetting_desc'] = 'With this setting you can define when the time controlled information banner should be hidden on the selected pages.
<br/>Please enter a valid date in this format: YYYY-MM-DD HH:MM:SS. For example: "2020-01-07 08:00:00. The time zone will be the time zone you have defined in the setting "Default timezone".
<br/>If you leave this setting empty but entered a date in the for the start, the banner won\'t hide after the starting time has been reached.';

// ADDITIONAL STRINGS (IN ALPHABETICAL ORDER).
$string['bootstrapprimarycolor'] = 'Primary color';
$string['bootstrapsecondarycolor'] = 'Secondary color';
$string['bootstrapsuccesscolor'] = 'Success color';
$string['bootstrapdangercolor'] = 'Danger color';
$string['bootstrapwarningcolor'] = 'Warning color';
$string['bootstrapinfocolor'] = 'Info color';
$string['bootstraplightcolor'] = 'Light color';
$string['bootstrapdarkcolor'] = 'Dark color';
$string['login_page'] = "Login page";

// Group management.
$string['groupsnotenabled'] = 'You can create groups, assign group members, and use them in single activities. To use groups course wide in all activities as default please change the <a href="{$a->coursesettings}">course settings</a>. More <a href="{$a->help}">info on groups</a>.';

$string['showhintcoursehiddengeneral'] = 'This course is currently <strong>hidden</strong>. Only enrolled managers can access this course when hidden.';
$string['messaging_warning'] = 'Note: Managers can potentially read anything written in a group chat by adding themselves to the group.';

// Login page strings.
$string['cookiesenabled'] = 'Cookies must be enabled in your browser to use Moodle.';
$string['username'] = 'Username';
$string['usernameemail'] = 'Username or mail address';
$string['rememberusername'] = 'Remember username';
$string['login'] = 'Login';
$string['separator_or'] = 'or';
$string['forgotten'] = 'Forgot login data?';

// Regions.
$string['region-footer-left'] = 'Footer (left)';
$string['region-footer-middle'] = 'Footer (middle)';
$string['region-footer-right'] = 'Footer (right)';
$string['region-footer-badge'] = 'Footer (badges)';
$string['region-side-pre'] = 'Right';
$string['region-above-content'] = 'Above content';
$string['region-login-mobile'] = 'Login (mobile)';
$string['region-login-desktop'] = 'Login (desktop)';